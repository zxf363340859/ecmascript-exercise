export default function countTypesNumber(source) {
  // TODO 6: 在这里写实现代码
  const arr = Object.values(source);
  console.log(arr);
  return arr
    .map(e => Number.parseInt(e))
    .reduce((total, num) => {
      return total + num;
    });
}
