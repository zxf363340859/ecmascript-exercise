function getScore(id) {
  if (id === '1234') {
    return 60;
  }
  return 59;
}

export default function getScoreOutput(person) {
  // TODO 8: 在这里写实现代码
  return '你好，'
    .concat(person.lastName)
    .concat(person.firstName)
    .concat('！')
    .concat('你的考试成绩为')
    .concat(getScore(person.id))
    .concat('分');
}
