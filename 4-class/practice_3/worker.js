// TODO 14: 在这里写实现代码
import Student from '../practice_2/student';

export default class Works extends Student {
  constructor(name, age, klass) {
    super(name, age);
    this.klass = klass;
  }

  introduce() {
    return `I am a Worker. I have a job.`;
  }
}
